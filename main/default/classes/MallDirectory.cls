public class MallDirectory {
    @AuraEnabled(cacheable=true)
        public static List < Object_A__c > getMallDetails() {

        return [SELECT Id, Name, Country__c, Location__c, (Select id, name from Store_Categories__r), (Select id, name from Brands__r) FROM Object_A__c];      
    }
   
}
