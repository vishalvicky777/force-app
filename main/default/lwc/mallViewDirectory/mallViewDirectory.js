import { LightningElement, track, wire } from 'lwc';
import { NavigationMixin } from 'lightning/navigation';
import getMallDetails from '@salesforce/apex/MallDirectory.getMallDetails';

export default class MallViewDirectory extends NavigationMixin( LightningElement ) {
    gridColumns = [
        { label: 'Mall', fieldName : 'Name', type: 'text',  sortable : false},
        { label: 'Country', fieldName : 'Country__c', type: 'text',  sortable : false},
        { label: 'Location', fieldName : 'Location__c', type: 'text',  sortable : false},
        {type: 'button',
        typeAttributes: {
            label: 'View'
        }
        }];
        
    gridData = []

    @wire(getMallDetails)
    mallViewData({ error, data }) {
        console.log( 'Inside wire' );
        if(data){
            this.gridData = data.map(item=>{
                const {Store_Categories__r, Brands__r, ...mall} = item
                const allBrands = Store_Categories__r.map(cat=>{
                    return {...cat, "_children":Brands__r}
                })
                return {...mall, "_children":allBrands}
            })

            console.log( 'Data is ' + JSON.stringify(  this.gridData ) );
        } 
        else if ( error ) {
            console.error(error);
        }

    }

    clickToExpandAll( e ) {
        const grid =  this.template.querySelector( 'lightning-tree-grid' );
        grid.expandAll();
    }

    clickToCollapseAll( e ) {

        const grid =  this.template.querySelector( 'lightning-tree-grid' );
        grid.collapseAll();
     
    }

    handleRowAction( event ) {
       
        const row = event.detail.row;
        this[NavigationMixin.Navigate]({
            type: 'standard__recordPage',
            attributes: {
                recordId: row.Id,
                actionName: 'view'
            }
        });

    }

}
